<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureTimingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up() {
        Schema::create('feature_timing', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('feature_id')->index();
            $table->enum('day',['sun','mon','tue','wed','thu','fri','sat']);
            $table->enum('status',[0,1])->comment('0->closed,1->open');
            $table->time('start_time');
            $table->time('end_time');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('feature_timing', function (Blueprint $table) {
            $table->foreign('feature_id')->references('id')->on('feature')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('feature_timing');
    }

}
