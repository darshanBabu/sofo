<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('connection_id')->index();
            $table->enum('mutual_on', ['fb', 'twitter', 'insta', 'linked'])->comment('which social profile is mutual');
            $table->enum('status', [1, 0])->comment('0->inactive connection,1->active connection');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('connections', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('app_users')->onDelete('cascade');
        });
        Schema::table('connections', function (Blueprint $table) {
            $table->foreign('connection_id')->references('id')->on('app_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('connections');
    }

}
