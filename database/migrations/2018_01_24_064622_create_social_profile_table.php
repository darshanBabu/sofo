<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialProfileTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('social_profiles', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->string('profile_id', 100);
            $table->string('image', 100);
            $table->enum('profile', ['fb', 'twitter', 'insta', 'linked'])->comment('which social profile is added');
            $table->enum('status', [1, 0])->comment('1->active,0->inactive');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('social_profiles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('app_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('social_profiles');
    }

}
