<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('added_by')->index();
            $table->unsignedInteger('contact_id')->index();
            $table->timestamps();
        });
        Schema::table('contact', function (Blueprint $table) {
            $table->foreign('contact_id')->references('id')->on('app_users')->onDelete('cascade');
        });
        Schema::table('contact', function (Blueprint $table) {
            $table->foreign('added_by')->references('id')->on('app_users')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}
