<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_users', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('device_id',100);
            $table->string('firebase_id',100);
            $table->string('first_name',50);
            $table->string('last_name',50)->nullable();
            $table->string('phone',50)->unique();
            $table->string('email',50)->unique();
            $table->string('address',200)->nullable();
            $table->string('user_name',50);
            $table->string('password',100);
            $table->string('profile_image',100)->nullable();
            $table->enum('online_status',[0,1])->comment('0->offline,1->online');
            $table->integer('otp');
            $table->integer('profile_completion');
            $table->string('latitude',10);
            $table->string('longitude',10);
            $table->enum('mode',['bus','god'])->comment('business/god modes');
            $table->softDeletes();                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_users');
    }
}
