<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureRatingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('feature_rating', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('rating');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('feature_id')->index();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('feature_rating', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('app_users')->onDelete('cascade');
        });
        Schema::table('feature_rating', function (Blueprint $table) {
            $table->foreign('feature_id')->references('id')->on('feature')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('feature_rating');
    }

}
