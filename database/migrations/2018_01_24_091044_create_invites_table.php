<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invites', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('invited_by')->index();
            $table->string('email', 100);
            $table->enum('joined', [1, 0])->comment('0->not joined,1->joined');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('invites', function (Blueprint $table) {
            $table->foreign('invited_by')->references('id')->on('app_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('invites');
    }

}
