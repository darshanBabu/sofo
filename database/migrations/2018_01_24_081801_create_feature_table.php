<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('feature_name');
            $table->string('feature_image',500);
            $table->string('feature_address',500);
            $table->string('feature_city');
            $table->string('feature_state');
            $table->unsignedInteger('feature_category_id')->index();
            $table->string('latitude');
            $table->string('longitude');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('feature', function (Blueprint $table) {
            $table->foreign('feature_category_id')->references('id')->on('feature_category')->onDelete('cascade');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature');
    }
}
