<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('checkin', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->unsignedInteger('feature_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('checkin', function (Blueprint $table) {
            $table->foreign('feature_id')->references('id')->on('feature')->onDelete('cascade');
        });
        Schema::table('checkin', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('app_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('checkin');
    }

}
