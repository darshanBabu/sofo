<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sofo Super Admin',
            'email' => 'superadmin@sofo.com',
            'role'=>'super',
            'password' => bcrypt('superadmin'),
            'created_at'=>date('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'name' => 'Sofo Admin',
            'email' => 'admin@sofo.com',
            'role'=>'admin',
            'password' => bcrypt('admin'),
            'created_at'=>date('Y-m-d H:i:s')
        ]);
    }
}
