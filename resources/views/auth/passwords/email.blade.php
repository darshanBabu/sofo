<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::to('assets/images/favicon.png') }}">
        <title>Sofo Login</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ URL::to('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ URL::to('css/style.css') }}" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{ URL::to('css/colors/blue.css') }}" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond') }} IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond') }} doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv') }}"></script>
        <script src="https://oss.maxcdn.com/libs/respond') }}/1.4.2/respond.min') }}"></script>
    <![endif]-->
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper" class="login-register login-sidebar"  style="background-image:url('../assets/images/background/login-register.jpg')">
            <div class="login-box card">
                <div class="card-block">

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        <br/><br/><br/><br/>
                        {{ csrf_field() }}    
                        <a href="javascript:void(0)" class="text-center db"><img src="{{ URL('assets/images/logo-light-icon.png') }}" alt="Home" /><br/><img src="{{ URL::to('assets/images/logo-light-text.png')}}" alt="Home" /></a>  
                        <br/><div class="form-group">                        
                            <label for="">Reset Password</label>
                            <input id="email" placeholder="Enter your E-Mail Address" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif                        
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                        </div>
                        <label><a href="/">Back to Login</a></label>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Send Password Reset Link</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="{{ URL::to('assets/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ URL::to('assets/plugins/bootstrap/js/tether.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ URL::to('js/jquery.slimscroll.js') }}"></script>
        <!--Wave Effects -->
        <script src="{{ URL::to('js/waves.js') }}"></script>
        <!--Menu sidebar -->
        <script src="{{ URL::to('js/sidebarmenu.js') }}"></script>
        <!--stickey kit -->
        <script src="{{ URL::to('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
        <!--Custom JavaScript -->
        <script src="{{ URL::to('js/custom.min.js') }}"></script>
    </body>

</html>