@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Manage Admin Profile</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Manage Admin Profile</h4>
                </div>
                <div class="card-block">
                    <form action="{{ route('editadminprofile') }}"  method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="adminname" class="form-control" value="{{Auth::user()->name}}"/>
                                            @if ($errors->has('adminname'))
                                            <small class="form-control-feedback"> {{ $errors->first('adminname') }} </small> 
                                            @endif 
                                        </div>                                       
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="adminmail" class="form-control" value="{{Auth::user()->email}}"/>
                                            @if ($errors->has('adminmail'))
                                            <small class="form-control-feedback"> {{ $errors->first('adminmail') }} </small> 
                                            @endif 
                                        </div>                                       
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Update</button>
                                    </div>
                                </div>                                
                                <!--/span-->
                            </div>
                            <!--/row-->                           
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection