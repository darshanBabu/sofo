@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
             <ol class="breadcrumb">
                            
                            <li class="breadcrumb-item active">Feature Listing</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <a href="{{ route('addEmployee') }}" class="btn waves-effect waves-light btn-success pull-right hidden-sm-down" title="Add Employee"> Add Employee</a>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block bg-info">
                    <h4 class="text-white card-title">Employees</h4>
                    <h6 class="card-subtitle text-white m-b-0 op-5">Checkout the employees here</h6>
                </div>
                <div class="card-block">
                    <div class="table-responsive m-t-10">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Edit/Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">S</span></td>
                                    <td>
                                        <h6>Hanna Gover</h6><small class="text-muted">Web Designer</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                                <tr>
                                    <td><span class="round"><img src="../assets/images/users/2.jpg" alt="user" width="50" /></span></td>
                                    <td>
                                        <h6>Andrew</h6><small class="text-muted">Project Manager</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                                <tr>
                                    <td><span class="round round-success">B</span></td>
                                    <td>
                                        <h6>Bhavesh patel</h6><small class="text-muted">Developer</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                                <tr>
                                    <td><span class="round round-primary">N</span></td>
                                    <td>
                                        <h6>Nirav Joshi</h6><small class="text-muted">Frontend Eng</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                                <tr>
                                    <td><span class="round round-warning">M</span></td>
                                    <td>
                                        <h6>Micheal Doe</h6><small class="text-muted">Content Writer</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                                <tr>
                                    <td><span class="round round-danger">N</span></td>
                                    <td>
                                        <h6>Johnathan</h6><small class="text-muted">Graphic</small></td>
                                    <td><a href="javascript:void(0)" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a> <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse" title="" data-toggle="tooltip" data-original-title="View"><i class="ti-eye"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection