@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Feature Category Listing</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <a href="{{ route('addfeaturecategory') }}" class="btn waves-effect waves-light btn-success pull-right hidden-sm-down" title="Add Feature Category"> Add Feature Category </a>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block bg-info">
                    <h4 class="text-white card-title">Feature Categories</h4>
                    <h6 class="card-subtitle text-white m-b-0 op-5">Checkout the Feature Categories here</h6>
                </div>
                <div class="card-block">
                    <div class="table-responsive m-t-10">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Edit/Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($categories)>0)
                                @foreach($categories as $category)
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">{{ $category->category_name[0] }}</span></td>
                                    <td>
                                        <h6>{{ $category->category_name }}</h6></td>
                                    <td><a href="{{ route('editfeaturecategory') }}/{{ $category->id }}" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete Category"><i data-id="{{ $category->id }}"class="ti-trash"></i></a>
                                </tr>                   
                                @endforeach
                                @else
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">N</span></td>
                                    <td>
                                        <h6>No Categories Found</h6></td>
                                    <td></td>
                                </tr>                               
                                @endif                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection
@push('script')
<script>
    $('.ti-trash').click(function () {
        var id = $(this).attr("data-id");
        if (confirm('Are you sure of deletion?')) {
            window.location = "{{ route('deletefeaturecategory') }}" + "/" + id;
        }
    });
</script>
@endpush