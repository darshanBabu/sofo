<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="{{ config('app.name') }}">
        <meta name="author" content="{{ config('app.name') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ URL('assets/images/favicon.png') }}">
        <title>Sofo Admin Panel</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ URL('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="{{ URL('assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
        <link href="{{ URL('assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
        <link href="{{ URL('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
        <!--This page css - Morris CSS -->
        <link href="{{ URL('assets/plugins/c3-master/c3.min.css') }}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ URL('css/style.css') }}" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{ URL('css/colors/blue.css') }}" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js') }} IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js') }}/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <header class="topbar">
                <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ route('employeeListing') }}">
                            <!-- Logo icon --><b>
                                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

                                <!-- Light Logo icon -->
                                <img src="{{ URL('assets/images/logo-light-icon.png') }}" alt="homepage" class="light-logo" />
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text --><span>

                                <!-- Light Logo text -->    
                                <img src="{{ URL('assets/images/logo-light-text.png') }}" class="light-logo" alt="homepage" /></span> </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-collapse">
                        <!-- ============================================================== -->
                        <!-- toggle and nav items -->
                        <!-- ============================================================== -->
                        <ul class="navbar-nav mr-auto mt-md-0">
                            <!-- This is  -->
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                            <!-- ============================================================== -->
                            <!-- Search -->
                            <!-- ============================================================== -->
                            <li class="nav-item hidden-sm-down search-box"> <!--<a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>-->
                                <form class="app-search">
                                  <!--  <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a>--> </form>
                            </li>
                        </ul>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <ul class="navbar-nav my-lg-0">
                            <!-- ============================================================== -->
                            <!-- Profile -->
                            <!-- ============================================================== -->
                            <!--                            <li class="nav-item dropdown">
                                                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL('assets/images/users/1.jpg') }}" alt="user" class="profile-pic m-r-10" />{{ Auth::user()->name }}</a>
                                                        </li>-->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ URL('assets/images/users/1.jpg') }}" alt="user" class="profile-pic m-r-10" />{{ Auth::user()->name }}</a>
                                <div class="dropdown-menu dropdown-menu-right scale-up">
                                    <ul class="dropdown-user">
                                        <li>
                                            <div class="dw-user-box">
                                                <div class="u-img"><img src="{{ URL('assets/images/users/1.jpg') }}" alt="user"></div>
                                                <div class="u-text">
                                                    <h4>{{ Auth::user()->name }}</h4>
                                                    <p class="text-muted">{{ Auth::user()->email }}</p><a href="{{ route('editprofile') }}" class="btn btn-rounded btn-danger btn-sm">Edit Profile</a></div>
                                            </div>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ route('employeeListing') }}"><i class="mdi mdi-account-check"></i> Team</a></li>
                                        <li><a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                                <i class="fa fa-power-off"></i> Logout
                                            </a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav"><br/><br/>
<!--                            <li> <a class="waves-effect waves-dark" href="{{ route('employeeListing') }}" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Team</span></a>
                            </li>-->
                            <li> <a class="waves-effect waves-dark" href="{{route('featurecategory')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Feature Categories</span></a>
                            </li>
                            <li> <a class="waves-effect waves-dark" href="{{route('featureListing')}}" aria-expanded="false"><i class="mdi mdi-earth"></i><span class="hide-menu">Features</span></a>
                            </li>                        
                            <li> <a class="waves-effect waves-dark" href="{{route('reportedusers')}}" aria-expanded="false"><i class="mdi mdi-help-circle"></i><span class="hide-menu">Reported Users</span></a>
                            </li>

                            <li> <a class="waves-effect waves-dark"><i class="fa fa-download"></i>
                                    <span class="hide-menu">Analytics Download</span>
                                </a>
                            </li>
                            <li> <a class="waves-effect waves-dark"><i class="fa fa-cloud-upload"></i>
                                    <span class="hide-menu">CSV file upload</span>
                                </a>
                            </li>
                        </ul>

                    </nav>
                    <!-- End Sidebar navigation -->
                </div>
                <!-- End Sidebar scroll-->
                <!-- Bottom points-->
                <div class="sidebar-footer">
                    <!-- item<a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>-->
                    <!-- item<a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>--> </div>
                <!-- End Bottom points-->
            </aside>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                @yield('content')
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <footer class="footer"> © SOFO Admin Panel </footer>
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="{{ URL('assets/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ URL('assets/plugins/bootstrap/js/tether.min.js') }}"></script>
        <script src="{{ URL('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ URL('js/jquery.slimscroll.js') }}"></script>
        <!--Wave Effects -->
        <script src="{{ URL('js/waves.js') }}"></script>
        <!--Menu sidebar -->
        <script src="{{ URL('js/sidebarmenu.js') }}"></script>
        <!--stickey kit -->
        <script src="{{ URL('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
        <!--Custom JavaScript -->
        <script src="{{ URL('js/custom.min.js') }}"></script>
        <!-- ============================================================== -->
        <!-- This page plugins -->
        <!-- ============================================================== -->
        <!-- chartist chart -->
        <!--<script src="{{ URL('assets/plugins/chartist-js/dist/chartist.min.js') }}"></script>-->
        <!--<script src="{{ URL('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>-->
        <!--c3 JavaScript -->
        <script src="{{ URL('assets/plugins/d3/d3.min.js') }}"></script>
        <script src="{{ URL('assets/plugins/c3-master/c3.min.js') }}"></script>
        <!-- Chart JS -->
        <!--<script src="{{ URL('js/dashboard1.js') }}"></script>-->
        @stack('script')
    </body>
</html>
