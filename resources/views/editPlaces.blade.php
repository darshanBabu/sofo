@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Features</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('featureListing') }}">Features</a></li>
                <li class="breadcrumb-item active">Edit Feature</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Edit Feature</h4>
                </div>
                <div class="card-block">
                    <form action="{{ route('storePlace') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="lat" name="latitude" value="{{ $featureDetails->latitude }}" readonly="yes"><br>
                        <input type="hidden" id="lng" name="longitude" readonly="yes" value="{{ $featureDetails->longitude }}">
                        <input type="hidden" id="edit"  readonly="yes" value="1">
                        <input type="hidden" name="id" value="{{ $featureDetails->id }}"/>
                        <input type="hidden" name="featureImage" value="{{ $featureDetails->feature_image }}"/>
                        <div class="form-body">
                            <h3 class="box-title m-t-10">Feature Info</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Feature Name</label>
                                        <input type="text" name="featurename" class="form-control" value="{{ $featureDetails->feature_name }}">
                                        @if ($errors->has('featurename'))
                                        <small class="form-control-feedback"> {{ $errors->first('featurename') }} </small> 
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Feature Address</label>
                                        <textarea name="featureaddress" id="featureaddress" colspan="4" rows="4" class="form-control">{{ $featureDetails->feature_address }}</textarea>
                                        @if ($errors->has('featureaddress'))
                                        <small class="form-control-feedback"> {{ $errors->first('featureaddress') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                                
                            </div>                            
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Feature Category</label>
                                        <select class="form-control custom-select" name="category">
                                            <option>--Select your Category--</option>
                                            @foreach($categories as $catDetails)
                                            <option  @if($featureDetails->feature_category_id==$catDetails->id)
                                                      selected="selected"
                                                      @endif
                                                      value="{{ $catDetails->id }}">{{ $catDetails->category_name }}</option>
                                            @endforeach
                                        </select>   
                                        @if ($errors->has('category'))
                                        <small class="form-control-feedback"> {{ $errors->first('category') }} </small> 
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label>Feature Image</label>
                                        <input type="file" name="image[]" class="form-control"/>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="card-title">Select a Place from the map</h4>
                                            <div id="map" class="gmaps"></div>
                                            @if ($errors->has('latitude'))
                                            <small class="form-control-feedback"> {{ $errors->first('category') }} </small> 
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <a href="{{ route('featureListing') }}"><button type="button" class="btn btn-inverse">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@push('script')
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCUBL-6KdclGJ2a_UpmB2LXvq7VOcPT7K4&sensor=true"></script>
<script src="{{ URL('assets/plugins/gmaps/gmaps.min.js') }}"></script>  
<script src="{{ URL('assets/plugins/gmaps/custom.gmaps.js') }}"></script>

@endpush
@endsection