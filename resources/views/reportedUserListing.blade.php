@extends('layout.app')
@section('content')
 <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Reported User Listing</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block bg-info">
                    <h4 class="text-white card-title">Reported User Listing</h4>
                    <h6 class="card-subtitle text-white m-b-0 op-5">Checkout the Reported User's here</h6>
                </div>
                <div class="card-block">
                    <div class="table-responsive m-t-10">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users)>0)
                                @foreach($users as $user)                                
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">{{ $user->users->first_name[0] }}</span></td>
                                    <td>
                                        <h6>{{ ucfirst($user->users->first_name) }}{{ $user->users->last_name }}</h6>
                                        <small class="text-muted">{{ $user->reason }}</small>
                                    </td>
                                    <td>
                                        <h6>{{ $user->users->email }}</h6>
                                        <small class="text-muted">Reported:{{ $user->attempts }} times</small>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="View Details"><i  class="fa fa-eye"></i></a>    
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Suspend"><i  class="fa fa-ban"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Block"><i  class="fa fa-bell-slash"></i></a>
                                    </td>
                                </tr>                               
                                @endforeach
                                @else
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">N</span></td>
                                    <td><h6>No Users</h6></td>
                                    <td></td>
                                </tr>  
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection
@push('script')
<script>
    $('.ti-trash').click(function () {
        var id=$(this).attr("data-id");
        if (confirm('Are you sure of deletion?')) {
            window.location = "{{ route('deleteFeature') }}" + "/"+id;
        }
    });
</script>
@endpush