@extends('layout.app')
@section('content')
<style>
    .form-control-feedback{
        color: #ff6666;
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Features</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('featureListing') }}">Features</a></li>
                <li class="breadcrumb-item active">Add Feature</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Add Feature</h4>
                </div>
                <div class="card-block">
                    <form action="{{ route('storePlace') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="lat" name="latitude" readonly="yes"><br>
                        <input type="hidden" id="lng" name="longitude" readonly="yes">
                        <input type="hidden" id="edit"  readonly="yes" value="0">
                        <div class="form-body">
                            <h3 class="box-title m-t-10">Feature Info</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Feature Name</label>
                                        <input type="text" name="featurename" id="featurename" class="form-control">
                                        @if ($errors->has('featurename'))
                                        <small class="form-control-feedback"> {{ $errors->first('featurename') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Feature Address</label>
                                        <textarea name="featureaddress" id="featureaddress" class="form-control"></textarea>
                                        @if ($errors->has('featureaddress'))
                                        <small class="form-control-feedback"> {{ $errors->first('featureaddress') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" name="featurecity" id="featurecity" class="form-control">
                                        @if ($errors->has('featurecity'))
                                        <small class="form-control-feedback"> {{ $errors->first('featurecity') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>State</label>
                                        <input type="text" name="featurestate" id="featurestate" class="form-control"/>
                                        @if ($errors->has('featurestate'))
                                        <small class="form-control-feedback"> {{ $errors->first('featurestate') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Feature Category</label>
                                        <select class="form-control custom-select" name="category">
                                            <option selected="" disabled="" value="" readonly="readonly">--Select your Category--</option>
                                            @foreach($categories as $catDetails)
                                            <option value="{{ $catDetails->id }}">{{ $catDetails->category_name }}</option>
                                            @endforeach
                                        </select>   
                                        @if ($errors->has('category'))
                                        <small class="form-control-feedback"> {{ $errors->first('category') }} </small> 
                                        @endif

                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label>Feature Image</label>
                                        <input type="file" name="image[]" class="form-control"/>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="card-title">Select a Place from the map</h4>
                                            <div id="map" class="gmaps"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <a href="{{ route('featureListing') }}"><button type="button" class="btn btn-inverse">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@push('script')
<script src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyCUBL-6KdclGJ2a_UpmB2LXvq7VOcPT7K4"></script>
<script src="{{ URL('assets/plugins/gmaps/gmaps.min.js') }}"></script>  
<script src="{{ URL('assets/plugins/gmaps/custom.gmaps.js') }}"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('featurename'));
        google.maps.event.addListener(places, 'place_changed', function () {            
            var place = places.getPlace();    
            $('#featurename').val(place.name);
            $('#featureaddress').val(place.formatted_address);
            for (var i = 0; i < place.address_components.length; i++) {
                if(place.address_components[i].types[0] == "locality"){
                    $('#featurecity').val(place.address_components[i].long_name);
                }
                if(place.address_components[i].types[0] == "administrative_area_level_1"){
                    $('#featurestate').val(place.address_components[i].long_name);
                }
            }            
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();            
            var myLatlng = new google.maps.LatLng(latitude, longitude); 
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 20,
                center: {lat: latitude, lng: longitude}
            });
            marker = new google.maps.Marker({
                map: map,
                position: myLatlng,
                draggable: true
            });
            document.getElementById('lat').value = latitude;
            document.getElementById('lng').value = longitude;
        });
    });
</script>
@endpush
@endsection