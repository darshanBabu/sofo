@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('employeeListing') }}">Employees</a></li>
                <li class="breadcrumb-item active">Add New Employee</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Add Employee</h4>
                </div>
                <div class="card-block">
                    <form action="{{ route('storeEmployee') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h3 class="card-title">Person Info</h3>
                            <hr>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <input type="text" id="name" name="name" class="form-control" placeholder="John doe" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                        <small class="form-control-feedback"> {{ $errors->first('name') }} </small> 
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" id="email" name="email" class="form-control" placeholder="John@doe.com" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                        <small class="form-control-feedback"> {{ $errors->first('email') }} </small> 
                                        @endif
                                    </div>
                                </div>
                                <!--/span-->
                            </div>                           
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control" value="{{ old('password') }}">
                                        @if ($errors->has('password'))
                                        <small class="form-control-feedback"> {{ $errors->first('password') }} </small> 
                                        @endif                                        
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select name="role" class="form-control custom-select">
                                            <option value="admin">Admin</option>
                                            <option value="employ" selected="seleceted">Employ</option>
                                        </select>
                                        @if ($errors->has('role'))
                                        <small class="form-control-feedback"> {{ $errors->first('role') }} </small>
                                        @endif                                        
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <a href="{{ route('employeeListing') }}"><button type="button" class="btn btn-inverse">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection