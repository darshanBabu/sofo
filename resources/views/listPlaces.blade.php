@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-4 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Features</li>
            </ol>
        </div>
        <div class="col-md-4 align-self-center">
            <form method="GET" action="{{ route('search.feature') }}">
                {{ csrf_field() }}    
                <div class="row">
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="searchkey" placeholder="Search Features.."/>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-success" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4 align-self-center">
            <a href="{{ route('addFeature') }}" class="btn waves-effect waves-light btn-success pull-right hidden-sm-down"> Add New Feature</a>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title m-b-0">Features</h4>
                </div>
                <div class="card-block collapse show">
                    <div class="table-responsive">
                        <table class="table product-overview">
                            <thead>
                                <tr>
                                    <th>Images</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Added On</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($features)>0)
                                @foreach($features as $feature)
                                <tr>
                                    <td>
                                        <img src="../assets/images/feature/{{ $feature->feature_image }}" alt="{{ $feature->feature_name }}" width="80">
                                    </td>
                                    <td>{{ $feature->feature_name }}</td>
                                    <td>{{ $feature->feature_address }}</td>
                                    <td>{{ date('d-m-Y',strtotime($feature->created_at)) }}</td>                                               
                                    <td><a href="{{ route('editPlace') }}/{{ $feature->id }}" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete Feature"><i class="ti-trash" data-id="{{ $feature->id }}"></i></a>
                                    </td>
                                </tr>    
                                @endforeach
                                @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>No Feature Found!!</td>
                                    <td></td>
                                </tr>    
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection
@push('script')
<script>
    $('.ti-trash').click(function () {
        var id=$(this).attr("data-id");
        if (confirm('Are you sure of deletion?')) {
            window.location = "{{ route('deleteFeature') }}" + "/"+id;
        }
    });
</script>
@endpush