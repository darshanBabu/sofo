@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('featurecategory') }}">Feature Categories</a></li>
                <li class="breadcrumb-item active">Add Feature Category</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Add Feature Category</h4>
                </div>
                <div class="card-block">
                    <form action="{{ route('storeFeature') }}"  method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <h3 class="card-title"> Feature Category Info</h3>
                            <hr>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Category Name</label>
                                        <input type="text" name="categoryname" class="form-control" value="{{ old('categoryname') }}" placeholder="Bar, Pubs, etc"/>
                                        @if ($errors->has('categoryname'))
                                        <small class="form-control-feedback"> {{ $errors->first('categoryname') }} </small> 
                                        @endif 
                                    </div>
                                </div>                                
                                <!--/span-->
                            </div>
                            <!--/row-->                           
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <a href="{{ route('featurecategory') }}"><button type="button" class="btn btn-inverse">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection