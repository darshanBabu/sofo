@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">Team Listing</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <a href="{{ route('addEmployee') }}" class="btn waves-effect waves-light btn-success pull-right hidden-sm-down" title="Add Employee"> Add Team</a>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-block bg-info">
                    <h4 class="text-white card-title">Team</h4>
                    <h6 class="card-subtitle text-white m-b-0 op-5">Checkout the employees here</h6>
                </div>
                <div class="card-block">
                    <div class="table-responsive m-t-10">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Edit/Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($employs)>0)
                                @foreach($employs as $list)                                
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">{{ $list->name[0] }}</span></td>
                                    <td>
                                        <h6>{{ ucfirst($list->name) }}</h6><small class="text-muted">{{ ucfirst($list->role) }}</small>
                                        <br/><small class="text-muted">{{ $list->email }}</small>
                                    </td>
                                    <td><a href="{{ route('editEmployee') }}/{{ $list->id }}" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a>
                                        <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-toggle="tooltip" data-original-title="Delete Employe"><i data-id="{{ $list->id }}" class="ti-trash"></i></a>                                            
                                    </td>
                                </tr>                               
                                @endforeach
                                @else
                                <tr class="active">
                                    <td style="width:50px;"><span class="round">N</span></td>
                                    <td><h6>No Users</h6></td>
                                    <td></td>
                                </tr>  
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection
@push('script')
<script>
    $('.ti-trash').click(function () {
        var id=$(this).attr("data-id");
        if (confirm('Are you sure of deletion?')) {
            window.location = "{{ route('deleteEmployee') }}" + "/"+id;
        }
    });
</script>
@endpush