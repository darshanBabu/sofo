
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ URL('assets/images/favicon.png') }}">
        <title>Sofo Login</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ URL('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ URL('css/style.css') }}" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{ URL('css/colors/blue.css') }}" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond') }} IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond') }} doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv') }}"></script>
        <script src="https://oss.maxcdn.com/libs/respond') }}/1.4.2/respond.min') }}"></script>
    <![endif]-->
    </head>

    <body>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper" class="login-register login-sidebar"  style="background-image:url('assets/images/background/login-register.jpg')">
            <div class="login-box card">
                <div class="card-block">
                    <form id="signin-form" method="post" enctype="multipart/form-data" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <a href="javascript:void(0)" class="text-center db"><img src="{{ URL('assets/images/logo-light-icon.png') }}" alt="Home" /><br/><img src="{{ URL('assets/images/logo-light-text.png')}}" alt="Home" /></a>  

                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" id="first-name" placeholder="Password" required="required">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                           <label for="checkbox-signup"> Remember me </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox pull-left p-t-0">
                                    <a href="{{ route('password.request') }}">Forgot Password</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-b-0">
                            <!--<div class="col-sm-12 text-center">
                                <p>Don't have an account? <a href="register2.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                            </div>-->
                        </div>
                    </form>
                    <form class="form-horizontal" id="recoverform" action="#">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <!--<h3>Recover Password</h3>
                                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>-->
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                               <!-- <input class="form-control" type="text" required="" placeholder="Email">-->
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <!--<button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="{{ URL('assets/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ URL('assets/plugins/bootstrap/js/tether.min.js') }}"></script>
        <script src="{{ URL('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ URL('js/jquery.slimscroll.js') }}"></script>
        <!--Wave Effects -->
        <script src="{{ URL('js/waves.js') }}"></script>
        <!--Menu sidebar -->
        <script src="{{ URL('js/sidebarmenu.js') }}"></script>
        <!--stickey kit -->
        <script src="{{ URL('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
        <!--Custom JavaScript -->
        <script src="{{ URL('js/custom.min.js') }}"></script>
    </body>

</html>