<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Feature;

class Places extends Controller {
    /*
     * list places
     */

    public function listPlaces(Request $request) {
        $data['features'] = App\Feature::with('category')
                ->orderBy('id', 'desc')
                ->get();
        return view('listPlaces', $data);
    }

    /*
     * edit a feature or place
     */

    public function editPlace($id) {
        $data['categories'] = App\FeatureCategory::all();
        $data['featureDetails'] = App\Feature::where('id', $id)
                ->first();
        return view('editPlaces', $data);
    }

    /*
     * Add Places
     */

    public function addPlaces() {
        $data['categories'] = App\FeatureCategory::all();
        return view('addPlaces', $data);
    }

    public function deletePlace($id) {
        $place = App\Feature::find($id);
        $place->delete();
        return redirect('user/featurelisting');
    }

    public function store(Request $request) {
        // Validate and store the place Details        
        if ($request->input('id')) {
            $place = App\Feature::find($request->input('id'));
        } else {
            $place = new App\Feature;
        }

        $validatedData = $request->validate([
        'featurename' => 'required',
        'featureaddress' => 'required',
        'featurecity' => 'required',
        'featurestate' => 'required',
        'category' => 'required',
        'latitude' => 'required',
        'longitude' => 'required'
        ]);
        /*
         * uploading image
         */
        $picture = '';
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $picture = date('His') . $filename;
                $destinationPath = base_path() . '\public\assets\images\feature';
                $file->move($destinationPath, $picture);
            }
        } else {
            if ($request->input('id')) {
                $picture = $request->input('featureImage');
            } else {
                $picture = "default/default.jpg";
            }
        }

        $place->feature_name = $request->input('featurename');
        $place->feature_image = $picture;
        $place->feature_address = $request->input('featureaddress');
        $place->feature_city = $request->input('featurecity');
        $place->feature_state = $request->input('featurestate');
        $place->feature_category_id = $request->input('category');
        $place->latitude = $request->input('latitude');
        $place->longitude = $request->input('longitude');
        if ($place->save()) {
            unset($place);
            return redirect('user/featurelisting');
        }
    }

    public function searchfeature(Request $request) {
        $searchkey = $request['searchkey'];
        $data['features'] = App\Feature::with('category')
                ->orderBy('id', 'desc')
                ->where('feature_name', 'like', '%' . $searchkey . '%')
                ->get();
        return view('listPlaces', $data);
    }

}
