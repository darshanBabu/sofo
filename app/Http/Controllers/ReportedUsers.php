<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class ReportedUsers extends Controller
{
    /*
     * reported user listing
     *
     */
    public function reportedUserListing(){
        $data['users']= App\ReportedUsers::with('users')
                                         ->get();
         return view('reportedUserListing',$data);
    }
}
