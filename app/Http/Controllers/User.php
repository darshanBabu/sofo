<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class User extends Controller {
    /*
     * User dashboard
     */

    public function dashboard() {
        //return view('dashboard');
    }

    public function editprofile() {
        if (Auth::user()->id) {
            return view('editprofile');
        } else {
            return redirect()->route('loginpage');
        }
    }

    public function editadminprofile(Request $request) {
        if (Auth::user()->id) {
            $validatedData = $request->validate([
            'adminname' => 'required',
            'adminmail' => 'required'
            ]);
            DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['name' => $request['adminname'], 'email' => $request['adminmail']]);
            return redirect()->back();
        } else {
            return redirect()->route('loginpage');
        }
    }

    public function login() {
        return view('login');
    }

}
