<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Hash;

class Employee extends Controller {
    /*
     * Employe listing
     */

    public function listing() {
        $data['employs'] = App\User::where('role', 'admin')
                ->orWhere('role', 'employ')
                ->orderBy('id', 'DESC')
                ->get();
        return view('employeListing', $data);
    }

    /*
     * Add employees
     */

    public function addEmployee() {
        return view('addEmployee');
    }

    public function editEmployee($id) {
        $data['employeDetails'] = App\User::where('id', $id)
                ->first();
        $data['id'] = $id;
        return view('editEmployee', $data);
    }

    public function deleteEmployee($id) {
        $employ = App\User::find($id);
        $employ->delete();
        return redirect('user/employeelisting');
    }

    public function store(Request $request) {
        // Validate and store the employees        
        if ($request->input('id')) {
            $user = App\User::find($request->input('id'));
        } else {
            $user = new App\User;
        }

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
        ]);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->password = Hash::make($request->input('password'));
        ;
        if ($user->save()) {
            return redirect('user/employeelisting');
        }
    }

}
