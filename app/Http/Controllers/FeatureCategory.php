<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class FeatureCategory extends Controller {

    public function listing() {
        $data['categories'] = App\FeatureCategory::all();
        return view('featureCategoryListing', $data);
    }

    public function addFeatureCategory() {
        return view('addFeatureCategory');
    }

    public function editFeatureCategory($id) {
        $data['categoryDetails'] = App\FeatureCategory::where('id', $id)
                ->first();
        $data['id'] = $id;
        return view('editFeatureCategory', $data);
    }

    /*
     * soft Delete FeatureCategory
     */

    public function deleteFeatureCategory($id) {
        $FeatureCategory = App\FeatureCategory::find($id);
        $FeatureCategory->delete();
        return redirect('user/featurecategory');
    }

    public function store(Request $request) {
        // Validate and store the FeatureCategory        
        if ($request->input('id')) {
            $category = App\FeatureCategory::find($request->input('id'));
        } else {
            $category = new App\FeatureCategory;
        }

        $validatedData = $request->validate([
            'categoryname' => 'required'
        ]);
        $category->category_name = $request->input('categoryname');
        if ($category->save()) {
            return redirect('user/featurecategory');
        }
    }

}
