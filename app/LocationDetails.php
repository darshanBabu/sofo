<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationDetails extends Model
{
    protected $table = 'location_details';
}
