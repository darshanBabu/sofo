<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureRating extends Model
{
    protected $table = 'feature_rating';
}
