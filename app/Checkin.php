<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model {

    protected $table = 'checkin';

    public function feature() {
        return $this->belongsTo('App/Feature', 'feature_id');
    }

    public function user() {
        return $this->belongsTo('App/AppUser', 'user_id');
    }

}
