<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportedUsers extends Model {

    protected $table = 'reported_users';

    public function users() {
        return $this->belongsTo('App\AppUser', 'user_id');
    }

}
