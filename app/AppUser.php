<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUser extends Model {

    protected $table = 'app_users';

    public function reportedUsers() {
        return $this->hasMany('App\ReportedUsers', 'user_id');
    }

    public function locations() {
        return $this->hasMany('App\LocationDetails', 'user_id');
    }

}
