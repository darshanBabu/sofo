<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model {

    protected $table = 'feature';

    use SoftDeletes;

    public function category() {
        return $this->belongsTo('App\FeatureCategory', 'feature_category_id');
    }

}
