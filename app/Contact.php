<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $table = 'contact';

    public function contact() {
        return $this->belongsTo('App\AppUser', 'contact_id');
    }

    public function addedBy() {
        return $this->belongsTo('App\AppUser', 'added_by');
    }

}
