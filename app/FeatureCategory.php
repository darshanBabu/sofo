<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeatureCategory extends Model {

    protected $table = 'feature_category';

    use SoftDeletes;

    public function feature() {
        return $this->hasMany('App\Feature', 'feature_category_id');
    }

}
