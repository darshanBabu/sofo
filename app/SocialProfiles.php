<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialProfiles extends Model {

    protected $table = 'social_profiles';

}
