<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureTiming extends Model
{
    protected $table = 'feature_timing';
}
