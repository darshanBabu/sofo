//map.js

//Set up some of our variables.
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 

//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
    /*
     * if a latitude and longitude value exists
     */
    var edit = document.getElementById('edit').value;
    var lat = document.getElementById('lat').value;
    var long = document.getElementById('lng').value;
    if (edit == 1)
    {
        var latLong = {lat: parseFloat(lat), lng: parseFloat(long)};
        var centerOfMap = new google.maps.LatLng(lat, long);

    } else {
        var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
    }
    /*
     * ends setting position while having latitude and longitude
     */
    //Map options.
    var options = {
        center: centerOfMap, //Set center.
        zoom: 10, //The zoom value.      
    };
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);
    if (edit == 1)
    {
        marker = new google.maps.Marker({
            position: latLong,
            map: map,
            draggable: true //make it draggable
        });
    } else {
        marker = false;
    }

    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function (event) {
        //alert(event.latLng);
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if (marker === false) {
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function (event) {
                markerLocation();
            });
        } else {
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}

//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation() {
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
}


//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);
/*
 * function for adding the latitude and longitude on giving the address uck
 */
$('#featureaddress').change(function () {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 20,
        center: {lat: -34.397, lng: 150.644}
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map);
    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('featureaddress').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                });
                document.getElementById('lat').value = results[0].geometry.location.lat(); //latitude
                document.getElementById('lng').value = results[0].geometry.location.lng(); //longitude

            } else {
                //alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
});