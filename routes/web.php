<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Auth::routes();

Route::get('/', 'User@login')->name('loginpage');

Route::middleware(['auth'])->prefix('user')->group(function () {
            //routes using the user as prefix, group if middlware priority needed after login	
            // Route::get('dashboard', 'User@dashboard')->name('dashboard');
            Route::get('employeelisting', 'Employee@listing')->name('employeeListing');
            Route::get('addemployee', 'Employee@addEmployee')->name('addEmployee');
            Route::get('editemployee/{id?}', 'Employee@editEmployee')->name('editEmployee');
            Route::get('deleteemployee/{id?}', 'Employee@deleteEmployee')->name('deleteEmployee');
            Route::get('featurecategory', 'FeatureCategory@listing')->name('featurecategory');
            Route::get('addfeaturecategory', 'FeatureCategory@addFeatureCategory')->name('addfeaturecategory');
            Route::get('deletefeaturecategory/{id?}', 'FeatureCategory@deleteFeatureCategory')->name('deletefeaturecategory');
            Route::get('editfeaturecategory/{id?}', 'FeatureCategory@editFeatureCategory')->name('editfeaturecategory');
            Route::get('addfeature', 'Places@addPlaces')->name('addFeature');
            Route::get('editplace/{id?}', 'Places@editPlace')->name('editPlace');
            Route::get('featurelisting', 'Places@listPlaces')->name('featureListing');
            Route::get('deletefeature/{id?}', 'Places@deletePlace')->name('deleteFeature');
            Route::get('reportedusers', 'ReportedUsers@reportedUserListing')->name('reportedusers');
            Route::get('editprofile', 'User@editprofile')->name('editprofile');
            Route::get('searchfeature', 'Places@searchfeature')->name('search.feature');


            Route::post('editadminprofile', 'User@editadminprofile')->name('editadminprofile');
            //storing
            Route::post('storeEmployee', 'Employee@store')->name('storeEmployee');
            Route::post('storeFeature', 'FeatureCategory@store')->name('storeFeature');
            Route::post('storePlace', 'Places@store')->name('storePlace');            
        });

